mkdir build

cd build

cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=lib -DWITH_PYTHONQT=OFF -DWITH_KF5DBus=OFF -DBoost_NO_BOOST_CMAKE=ON -DWEBVIEW_FORCE_WEBKIT=OFF -DSKIP_MODULES="webview tracking interactiveterminal initramfs nitramfscfg dracut dracutlukscfg  dummyprocess dummypython dummycpp dummypythonqt services-openrc keyboardq localeq welcomeq"

cd ..

make

# binary needs to populate config files in / so in order to run it you need to pass
sudo make install

# then copy the etc folder in this repo to /etc
